package com.kaarthick.jms.listener;

import javax.jms.Message;
import javax.jms.MessageListener;

import org.springframework.stereotype.Component;

@Component
public class ConsumerListener implements MessageListener { //MDB 

	public void onMessage(Message queueMessage) {
		System.out.println("In onMessage method " + queueMessage);
	}

}
